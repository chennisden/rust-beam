# rust-beam

beam is a plaintext slideshow format you can write in faster than beamer.

This is the Rust implementation of the informal beam specification.

## Dependencies

### Build

Rust stable.

### Runtime

A working installation of LaTeX with beamer.

## Installation

Run `cargo install rust-beam` to install the latest stable version of rust-beam.

Run `cargo install --git https://gitlab.com/chennisden/rust-beam.git` to install rust-beam from source.

## Specs

Here is an informal specification for the beam file format.

Slides
- Slides are separated by a `~` mark. Whatever text follows `~` is the title of
  the slide. Leave no text after `~` if you do not want to set a title.
- Leading and trailing whitespace in a line is ignored.

Special characters
- `#` will start a new section, and the title will be the text that follows it
  on the same line.
- `~` will create a new slide, and the title will be the text that follows it
  on the same line.
- `-` can be used to make a list of bullet points. Under the hood, it calls the
	"itemize" environment in LaTeX.
- `>` starts a block. The first line is used as the title (leave it blank for no title).
- `<` starts an exampleblock. Again, the first line is used as the title.
- `!` starts an alertblock. Again, the first line is used as the title.
- `@` can be used to create a slide with a certain image. It is possible, but
	discouraged, to add text to such a slide. Under the hood, it uses
	```
	\setbeamertemplate{background}{
		\includegraphics[width=\paperwidth,height=\paperheight]{IMAGEPATH}
	}
	```
- `*` can be used to insert an image into the slide. Under the hood, it calls
	```
	\begin{center}
		\includegraphics[width=6cm]{IMAGEPATH}
	\end{center}
	```
- `^` will directly input text verbatim into the preamble. This lets you set
	the theme, title, and author of a presentation. If a line starts with `^`, it
	will end the current slide, without exception.

- Character escaping is handled by LaTeX, not beam. That means it's impossible
    to display a line that starts with a raw `-`, `!`, `>`, or `<` character.
    This is regarded as a design flaw, but a very minor one.

You can also use % to act as a comment, since it's also a comment in LaTeX. Be
careful not to start a new slide by accident though.

## Examples

All examples can be found in `examples/`, and it is assumed that you are in that directory.

An example is contained in "example.beam". Provided that `rust-beam` is on your
system, run

	rust-beam example.beam

beam will generate an intermediate TeX file and then compile it through
`latexmk`. The example is reproduced below.

	^ \documentclass{beamer}[12pt]
	^ \title{beam}
	^ \author{Dennis Chen}

	\titlepage

	# Introduction

	~ What is beam?
	- beam is a file format to write presentations with
	- beam is a program that converts beam files into beamer

	~ Why?
	beam does for beamer what Markdown does for HTML
	- beamer takes far too long to write.
	- We only need a subset of beamer's features

	# Features
	
	~ Frametitles
	Use the ``~'' character to set the frametitle.

	~ Bullet points
	- You've already seen it in this presentation.
	- Bullet points invoke the ``itemize'' environment.

	~ Block
	> How to make a block
	> This symbol makes a block.

	~ Exampleblock
	< How to make an exampleblock
	< This symbol makes an exampleblock.

	~ Alertblock
	! How to make an alertblock
	! This symbol makes an alertblock.

	~ Images
	Use the ``\@'' character to set a background image.

    ~
	@beach.jpg

    ~
	You can also use the ``\*'' characater to include an image in the slide.
	*beach.jpg

	# Miscellaneous

	~ Stylization
	beam should be written in all lowercase, even at the start of a sentence. 
